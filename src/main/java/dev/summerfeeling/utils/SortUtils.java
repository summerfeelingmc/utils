package dev.summerfeeling.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;

public class SortUtils {

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> toSort) {
        Map<K, V> result = new LinkedHashMap<>();
        toSort.entrySet().stream().sorted(Collections.reverseOrder(Entry.comparingByValue())).forEachOrdered(entry -> result.put(entry.getKey(), entry.getValue()));
        return result;
    }

    public static <K extends Comparable<? super K>, V> Map<K, V> sortByKeys(Map<K, V> toSort) {
        Map<K, V> results = new LinkedHashMap<>();
        toSort.entrySet().stream().sorted(Collections.reverseOrder(Entry.comparingByKey())).forEachOrdered(entry -> results.put(entry.getKey(), entry.getValue()));
        return results;
    }

    public static <K, V extends Comparable<? super V>> LinkedList<K> keysSortedByValue(Map<K, V> toSort) {
        LinkedList<K> result = new LinkedList<>();
        toSort.entrySet().stream().sorted(Collections.reverseOrder(Entry.comparingByValue())).forEachOrdered(entry -> result.add(entry.getKey()));
        return result;
    }

    public static <K extends Comparable<? super K>, V> Map<K, V> reverseByKeys(Map<K, V> toSort) {
        Map<K, V> results = new LinkedHashMap<>();
        toSort.entrySet().stream().sorted(Entry.comparingByKey()).forEachOrdered(entry -> results.put(entry.getKey(), entry.getValue()));
        return results;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> reverseByValue(Map<K, V> toSort) {
        Map<K, V> result = new LinkedHashMap<>();
        toSort.entrySet().stream().sorted(Entry.comparingByValue()).forEachOrdered(entry -> result.put(entry.getKey(), entry.getValue()));
        return result;
    }

}
