package dev.summerfeeling.utils.reflect;

import java.lang.reflect.Field;

public interface FieldAccessor {

    <T> T get(Object instance);

    default <T> T getStatic() {
        return get(null);
    }

    default void setStatic(Object value) {
        set(null, value);
    }

    void set(Object instance, Object value);

    Field getField();

}
