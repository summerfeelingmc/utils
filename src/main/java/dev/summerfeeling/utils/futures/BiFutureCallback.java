package dev.summerfeeling.utils.futures;

/**
 * Created: 17.12.2020
 *
 * @author Summerfeeling
 */
public interface BiFutureCallback<A, B> {

    void onSuccess(A a, B b);
    void onFailure(Throwable throwable);

}
