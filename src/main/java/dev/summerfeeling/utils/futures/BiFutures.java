package dev.summerfeeling.utils.futures;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.Executor;

/**
 * Created: 17.12.2020
 *
 * @author Summerfeeling
 */
public class BiFutures {

    public static <A, B> void addCallback(ListenableFuture<A> a, ListenableFuture<B> b, BiFutureCallback<A, B> callback, Executor executor) {
        Futures.addCallback(a, new FutureCallback<A>() {
            @Override
            public void onSuccess(A a) {
                Futures.addCallback(b, new FutureCallback<B>() {
                    @Override
                    public void onSuccess(B b) {
                        callback.onSuccess(a, b);
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        callback.onFailure(throwable);
                    }
                }, executor);
            }

            @Override
            public void onFailure(Throwable throwable) {
                executor.execute(() -> callback.onFailure(throwable));
            }
        }, MoreExecutors.directExecutor());
    }

    public static <A, B> void addCallback(ListenableFuture<A> a, ListenableFuture<B> b, BiFutureCallback<A, B> callback) {
        BiFutures.addCallback(a, b, callback, MoreExecutors.directExecutor());
    }

}
