package dev.summerfeeling.utils;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DateUtils {

    private static final Pattern timePattern = Pattern.compile(
        "(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?" +
            "(?:([0-9]+)\\s*(?:s[a-z]*)?)?",
        Pattern.CASE_INSENSITIVE
    );

    /**
     * <pre>
     * Credits to Essentials.
     *
     * Parse the given String "time" into a long.
     *
     * Example: 9mo12w3d -&gt; 9 months, 12 weeks and 3 days.
     * y = years
     * mo = months
     * w = weeks
     * d = days
     * h = hours
     * m = minutes
     *
     * Maximum: 20 years.
     * </pre>
     *
     * @param time   The time string
     * @param future Whether the time is in the future
     * @return the given "time" string in milliseconds.
     * @throws Exception If something goes wrong
     */
    public static long parseDateString(String time, boolean future) throws Exception {
        Matcher matcher = timePattern.matcher(time);

        int years = 0,
            months = 0,
            weeks = 0,
            days = 0,
            hours = 0,
            minutes = 0,
            seconds = 0;

        boolean found = false;

        while (matcher.find()) {
            if (matcher.group() == null || matcher.group().isEmpty()) {
                continue;
            }

            for (int i = 0; i < matcher.groupCount(); i++) {
                if (matcher.group(i) != null && !matcher.group(i).isEmpty()) {
                    found = true;
                    break;
                }
            }

            if (found) {
                if (matcher.group(1) != null && !matcher.group(1).isEmpty()) {
                    years = Integer.parseInt(matcher.group(1));
                }

                if (matcher.group(2) != null && !matcher.group(2).isEmpty()) {
                    months = Integer.parseInt(matcher.group(2));
                }

                if (matcher.group(3) != null && !matcher.group(3).isEmpty()) {
                    weeks = Integer.parseInt(matcher.group(3));
                }

                if (matcher.group(4) != null && !matcher.group(4).isEmpty()) {
                    days = Integer.parseInt(matcher.group(4));
                }

                if (matcher.group(5) != null && !matcher.group(5).isEmpty()) {
                    hours = Integer.parseInt(matcher.group(5));
                }

                if (matcher.group(6) != null && !matcher.group(6).isEmpty()) {
                    minutes = Integer.parseInt(matcher.group(6));
                }

                if (matcher.group(7) != null && !matcher.group(7).isEmpty()) {
                    seconds = Integer.parseInt(matcher.group(7));
                }
            }
        }

        if (!found) {
            throw new Exception("Illegal Date given!");
        }

        if (years > 20) {
            throw new Exception("Illegal Date given!");
        }

        Calendar calendar = new GregorianCalendar();

        if (years > 0) {
            calendar.add(Calendar.YEAR, years * (future ? 1 : -1));
        }

        if (months > 0) {
            calendar.add(Calendar.MONTH, months * (future ? 1 : -1));
        }

        if (weeks > 0) {
            calendar.add(Calendar.WEEK_OF_YEAR, weeks * (future ? 1 : -1));
        }

        if (days > 0) {
            calendar.add(Calendar.DAY_OF_MONTH, days * (future ? 1 : -1));
        }

        if (hours > 0) {
            calendar.add(Calendar.HOUR_OF_DAY, hours * (future ? 1 : -1));
        }

        if (minutes > 0) {
            calendar.add(Calendar.MINUTE, minutes * (future ? 1 : -1));
        }

        if (seconds > 0) {
            calendar.add(Calendar.SECOND, seconds * (future ? 1 : -1));
        }

        return calendar.getTimeInMillis();
    }

}
