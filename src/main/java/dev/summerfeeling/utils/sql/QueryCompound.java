package dev.summerfeeling.utils.sql;

public interface QueryCompound {

    StringBuilder appendToWhere(StringBuilder builder);

}
