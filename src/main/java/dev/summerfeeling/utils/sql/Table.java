package dev.summerfeeling.utils.sql;

import com.google.common.base.Joiner;
import dev.summerfeeling.utils.sql.argument.ArgumentShape;
import dev.summerfeeling.utils.sql.argument.TableArgument;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Table {

    private static final Joiner COMMA_JOINER = Joiner.on(',');

    private final Database database;
    private final String name;
    private final String rawName;

    private final List<String> columnNames;

    public Table(Database base, String name) {
        this.database = base;
        this.name = '`' + name + '`';
        this.rawName = name;

        this.columnNames = new ArrayList<>();

        final ResultSet set = database.sendQuery("DESC " + this.name);

        try {
            while (set.next()) {
                this.columnNames.add(set.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Database getDatabase() {
        return database;
    }

    private StringBuilder createStringBuilder(TableArgument argument) {
        if (columnNames.contains(argument.getName())) {
            return null;
        }

        StringBuilder builder = new StringBuilder("ALTER TABLE ").append(name).append(" ADD ").append(argument.getName()).append(" ").append(argument.getType().getValue()).append(" ");

        for (ArgumentShape shape : argument.getShapes()) {
            builder.append(shape.getValue()).append(" ");
        }

        return builder;
    }

    public void addColumn(TableArgument argument, boolean first) {
        StringBuilder builder = createStringBuilder(argument);
        if (builder == null) return;

        builder.append(first ? "FIRST" : "").append(";");

        this.database.sendUpdate(builder.toString());
        this.columnNames.add(argument.getName());
    }

    public void addColumn(TableArgument argument, String after) {
        StringBuilder builder = createStringBuilder(argument);
        if (builder == null) return;

        builder.append("AFTER ").append(after).append(";");

        this.database.sendUpdate(builder.toString());
        this.columnNames.add(argument.getName());
    }

    public void removeColumn(String name) {
        if (!columnNames.contains(name)) {
            return;
        }

        this.database.sendUpdate("ALTER TABLE " + this.name + " DROP COLUMN " + name);
        this.columnNames.remove(name);
    }

    public String getName() {
        return name;
    }

    public String getRawName() {
        return rawName;
    }

    public ResultSet getContent() {
        return database.sendQuery("SELECT * FROM " + name);
    }

    public String[] getColumnNames() {
        return columnNames.toArray(new String[0]);
    }

    public boolean removeAllAt(SqlIdentifier first, SqlIdentifier... others) {
        StringBuilder builder = new StringBuilder("DELETE FROM ").append(name).append(" ");
        this.appendWhere(builder, first, others);

        return database.sendUpdate(builder.toString());
    }

    public boolean add(String[] columns, Object[] objects) {
        return add(false, columns, objects);
    }

    public boolean add(boolean onlyIfNotExists, Object[] columns, Object[] objects) {
        final StringBuilder builder = new StringBuilder("INSERT ");

        if (onlyIfNotExists) builder.append("IGNORE ");

        builder.append("INTO ").append(name).append(' ');
        builder.append('(').append(COMMA_JOINER.join(columns));
        builder.append(") VALUES (");

        for (int i = 0; i < objects.length; i++) {
            builder.append('\'').append(objects[i]).append('\'');

            if (i != objects.length - 1) {
                builder.append(',');
            }
        }

        builder.append(')');

        return database.sendUpdate(builder.toString());
    }

    public boolean addOrUpdate(String[] columns, Object[] values) {
        StringBuilder builder = new StringBuilder("INSERT INTO ").append(name).append(' ').append('(').append(COMMA_JOINER.join(columns)).append(") VALUES (");

        for (int i = 0; i < values.length; i++) {
            builder.append('\'').append(values[i]).append('\'');

            if (i != values.length - 1) {
                builder.append(',');
            }
        }

        builder.append(") ON DUPLICATE KEY UPDATE ");
        this.appendSetFromValues(builder, columns);

        return database.sendUpdate(builder.toString());
    }

    public boolean isSet(SqlIdentifier first, SqlIdentifier... others) {
        try {
            StringBuilder builder = new StringBuilder("SELECT count(*) FROM ").append(name);
            this.appendWhere(builder, first, others);

            final ResultSet set = database.sendQuery(builder.toString());

            if (set.next()) {
                return set.getInt(1) != 0;
            }

            return false;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet compoundGet(QueryCompound... queryCompounds) {
        return database.sendQuery(getQuery("SELECT *", queryCompounds));
    }

    public boolean compoundDelete(QueryCompound... queryCompounds) {
        return database.sendUpdate(getQuery("DELETE", queryCompounds));
    }

    private String getQuery(String query, QueryCompound... queryCompounds) {
        final StringBuilder builder = new StringBuilder(query).append(" FROM ").append(name).append(" WHERE");

        for (QueryCompound compound : queryCompounds) {
            builder.append(' ');
            compound.appendToWhere(builder);
        }

        return builder.toString();
    }

    public ResultSet get(SqlIdentifier identifier) {
        return get(identifier, "*");
    }

    public ResultSet get(SqlIdentifier identifier, String... columnName) {
        return get(identifier, null, columnName);
    }

    public ResultSet get(SqlIdentifier first, SqlIdentifier second, String... columnName) {
        final StringBuilder builder = new StringBuilder("SELECT ").append(COMMA_JOINER.join(columnName)).append(" FROM ").append(name);

        if (second == null) {
            this.appendWhere(builder, first);
        } else {
            this.appendWhere(builder, first, second);
        }

        return database.sendQuery(builder.toString());
    }

    public int getSize() {
        final ResultSet set = database.sendQuery("SELECT COUNT(*) FROM " + name);

        try {
            if (set.next()) {
                return set.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public int getNotNullSize(String column) {
        final ResultSet set = database.sendQuery("SELECT COUNT(*) FROM " + name + " WHERE `" + column + "` IS NOT NULL");

        try {
            if (set.next()) {
                return set.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public int countWhere(SqlIdentifier first, SqlIdentifier... others) {
        StringBuilder builder = new StringBuilder("SELECT COUNT(*) FROM ").append(name);
        this.appendWhere(builder, first, others);

        final ResultSet set = database.sendQuery(builder.toString());

        try {
            if (set.next()) {
                return set.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return -1;
    }

    public boolean set(Object value, String columnName) {
        return set(value, columnName, null);
    }

    public boolean set(Object value, String columnName, SqlIdentifier first, SqlIdentifier... others) {
        StringBuilder builder = new StringBuilder("UPDATE ").append(name).append(" SET ").append(columnName).append(" = '").append(value).append("' ");

        if (first != null) {
            this.appendWhere(builder, first, others);
        }

        this.database.sendUpdate(builder.toString());

        return true;
    }

    public void set(Object[] value, String[] columnName, SqlIdentifier first, SqlIdentifier... others) {
        if (value.length != columnName.length) {
            throw new IllegalArgumentException("Value length is not equal to columnName length!");
        }

        final StringBuilder builder = new StringBuilder("UPDATE ").append(name).append(" SET ");

        this.appendSet(builder, columnName, value);
        this.appendWhere(builder, first, others);

        this.database.sendUpdate(builder.toString());
    }

    public boolean add(int amount, String columnName) {
        return add(amount, columnName, null);
    }

    public boolean add(int amount, String columnName, SqlIdentifier first, SqlIdentifier... others) {
        StringBuilder builder = new StringBuilder("UPDATE ").append(name).append(" SET ").append(columnName).append(" = ").append(columnName).append(" + ").append(amount).append(' ');

        if (first != null) {
            this.appendWhere(builder, first, others);
        }

        this.database.sendUpdate(builder.toString());

        return true;
    }

    public void clear() {
        database.sendUpdate("TRUNCATE " + name);
    }

    private StringBuilder appendWhere(StringBuilder builder, SqlIdentifier first, SqlIdentifier... others) {
        builder.append(" WHERE ");
        first.appendToWhere(builder);

        for (int i = others.length - 1; i >= 0; i--) {
            builder.append(" AND ");
            others[i].appendToWhere(builder);
        }

        return builder;
    }

    private StringBuilder appendSet(StringBuilder builder, String[] columns, Object[] values) {
        for (int i = 0; i < values.length; i++) {
            builder.append(columns[i]).append('=').append('\'').append(values[i]).append('\'');

            if (i != values.length - 1) {
                builder.append(',');
            }
        }

        return builder;
    }

    private StringBuilder appendSetFromValues(StringBuilder builder, String[] columns) {
        for (int i = 0; i < columns.length; i++) {
            builder.append(columns[i]).append('=').append("VALUES(").append(columns[i]).append(')');

            if (i != columns.length - 1) {
                builder.append(',');
            }
        }

        return builder;
    }

    @Override
    public String toString() {
        return String.format("Table{name=%s,database=%s}", rawName, database.getName());
    }

}