package dev.summerfeeling.utils.sql;

import java.util.Random;

public class IDGenerator {

    private static final char[] POSSIBLE_CHARACTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
    private static final Random RANDOM = new Random();

    public static String generateID(Table table, String column, int chars) {
        String id = randomAlphanumeric(chars);

        if (!table.isSet(new SqlIdentifier(id, column))) {
            return id;
        }

        return generateID(table, column, chars);
    }

    public static String generateID(Table table, int chars) {
        return generateID(table, "id", chars);
    }

    private static String randomAlphanumeric(int chars) {
        StringBuilder random = new StringBuilder();

        for (int i = 0; i < chars; i++) {
            random.append(POSSIBLE_CHARACTERS[RANDOM.nextInt(POSSIBLE_CHARACTERS.length)]);
        }

        return random.toString();
    }

}
