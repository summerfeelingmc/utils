package dev.summerfeeling.utils.sql;

import com.google.common.base.Joiner;
import dev.summerfeeling.utils.sql.argument.TableArgument;
import io.netty.util.internal.RecyclableArrayList;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.*;
import java.util.*;

public class Database {

    public static final int DEFAULT_MAX_VERBOSE_LENGTH = 70;
    public static final long TIMEOUT = 10 * 60 * 1000;

    private static final Map<String, Connection> CREATED_CONNECTIONS = new HashMap<>();
    private static final Map<String, Database> CREATED_DATABASES = new HashMap<>();

    private static final Joiner COMMA_JOINER = Joiner.on(',');
    private static final Object MONITOR = new Object();

    private final String host;
    private final int port;

    private final String baseName;
    private final String userName;
    private final String password;

    protected Set<Table> tableSet;
    protected Connection connection;
    protected long lastUpdate;
    private boolean verboseEnabled;
    private int verboseMaxLength;

    public Database(String host, int port, String userName, String password, String baseName) throws IllegalArgumentException {
        this.host = host;
        this.port = port;
        this.baseName = baseName;
        this.userName = userName;
        this.password = password;

        if (CREATED_CONNECTIONS.containsKey(host)) {
            this.connection = CREATED_CONNECTIONS.get(host);
        }

        try {
            this.tableSet = new HashSet<>();

            final ResultSet rs = this.getConnection().getMetaData().getTables(getConnection().getCatalog(), null, "%", null);

            while (rs.next()) {
                this.tableSet.add(new Table(this, rs.getString(3)));
            }

            Database.CREATED_CONNECTIONS.put(host, connection);
        } catch (SQLException e) {
            System.out.println("Error while connecting database " + baseName + " @ " + host);
            e.printStackTrace();

            throw new IllegalArgumentException(e.getMessage());
        }

        Database.CREATED_DATABASES.put(baseName.toLowerCase(), this);
    }

    public static boolean isReady(Database database) {
        return database != null && database.ping() != -1;
    }

    public static Database getDatabase(String name) {
        return CREATED_DATABASES.get(name.replace(" ", "_").toLowerCase());
    }

    public static void closeAll() {
        final Iterator<Database> it = CREATED_DATABASES.values().iterator();

        while (it.hasNext()) {
            try {
                it.next().close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Database enableVerbose(int verboseMaxLength) {
        this.verboseEnabled = true;
        this.verboseMaxLength = verboseMaxLength;
        return this;
    }

    public Database enableVerbose() {
        return enableVerbose(DEFAULT_MAX_VERBOSE_LENGTH);
    }

    public Database disableVerbose() {
        this.verboseEnabled = false;
        return this;
    }

    public String getHost() {
        return this.host;
    }

    public int getPort() {
        return this.port;
    }

    public String getName() {
        return this.baseName;
    }

    public Connection getConnection() throws SQLException {
        try {
            synchronized (MONITOR) {
                if (this.connection != null && this.connection.isValid(1) && System.currentTimeMillis() - this.lastUpdate < Database.TIMEOUT) {
                    return this.connection;
                }

                if (this.connection != null) {
                    this.connection.close();
                }

                this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.baseName + "?characterEncoding=utf8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", this.userName, this.password);
                this.lastUpdate = System.currentTimeMillis();

                Database.CREATED_CONNECTIONS.put(this.host, connection);

                return this.connection;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public synchronized void close() throws SQLException {
        if (connection != null) {
            this.connection.close();
            this.connection = null;
        }
    }

    public ResultSet sendQuery(String query) {
        try {
            verbose(query);

            return this.getNewStatement().executeQuery(query);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean sendUpdate(String update) {
        try {
            verbose(update);

            this.getNewStatement().executeUpdate(update);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void verbose(String update) {
        if (verboseEnabled) {
            String tmp = update;

            if (update.length() >= verboseMaxLength) {
                tmp = update.substring(0, verboseMaxLength - 1) + " [...]";
            }

            StackTraceElement caller = getCaller();
            System.out.println((caller != null ? caller.toString() : null) + " sendet SQL-Update: " + tmp);
        }
    }

    public Statement getNewStatement() {
        try {
            return this.getConnection().createStatement();
        } catch (final SQLException e) {
            return null;
        }
    }

    public Table[] getTables() {
        return this.tableSet.toArray(new Table[0]);
    }

    public long ping() {
        final long start = System.currentTimeMillis();

        try {
            this.getConnection().createStatement().executeQuery("SELECT 1");
            return System.currentTimeMillis() - start;
        } catch (Exception e) {
            return -1;
        }
    }

    public Table getTable(String name) {
        final Iterator<Table> tableIt = this.tableSet.iterator();

        if (!name.startsWith("`")) {
            name = "`" + name;
        }

        if (!name.endsWith("`")) {
            name = name + "`";
        }

        Table table;

        while (tableIt.hasNext()) {
            table = tableIt.next();

            if (table.getName().equalsIgnoreCase(name)) {
                return table;
            }
        }

        return null;
    }

    public Table createTable(String name, TableArgument[] arguments, boolean force) {
        final StringBuilder builder = new StringBuilder("CREATE TABLE ");
        if (!force) builder.append("IF NOT EXISTS ");

        builder.append('`').append(name).append('`').append(' ').append('(');

        final RecyclableArrayList primaries = RecyclableArrayList.newInstance();

        for (int i = arguments.length - 1; i >= 0; i--) {
            if (arguments[i].isPrimary()) {
                primaries.add(arguments[i].getName());
            }

        }

        builder.append(COMMA_JOINER.join(arguments));

        if (!primaries.isEmpty()) {
            builder.append(", PRIMARY KEY (");
            builder.append(COMMA_JOINER.join(primaries));
            builder.append(')');
        }
        primaries.recycle();
        builder.append(')');

        try {
            this.getNewStatement().executeUpdate(builder.toString());

            System.out.println("name = " + name);
            final Table table = new Table(this, name);
            this.tableSet.add(table);

            return table;
        } catch (final SQLException e) {
            return null;
        }
    }

    public boolean removeTable(String name) {
        try {
            final Table table = this.getTable(name);

            if (table != null) {
                this.getNewStatement().executeUpdate("DROP TABLE " + name);
                this.tableSet.remove(table);

                return true;
            }

            return false;
        } catch (SQLException e) {
            return false;
        }
    }

    private StackTraceElement getCaller() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String mysqlPackage = getClass().getPackage().getName();

        StackTraceElement caller = null;

        for (int i = 1; i < stackTrace.length; i++) {
            if (!(caller = stackTrace[i]).getClassName().startsWith(mysqlPackage)) {
                break;
            }
        }

        return caller;
    }

    private String getHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        return "unkown";
    }

    @Override
    public String toString() {
        return "{Host: " + host + ":" + port + ", User: " + userName + ", BaseName: " + baseName + "}";
    }
}
