package dev.summerfeeling.utils.sql;

public class SqlIdentifier implements QueryCompound {

    protected String columnName;
    protected String idName;

    public SqlIdentifier(Object value, String columnName) {
        this(value.toString(), columnName);
    }

    public SqlIdentifier(String value, String columnName) {
        this.columnName = columnName;
        this.idName = value;

        if (this.idName.startsWith("'") || this.idName.endsWith("'")) {
            new Exception("idName in SqlIdentifier already escaped. Shouldn't be the case.").printStackTrace();
            this.idName = this.idName.replace("'", "");
        }
    }

    public String getColumnName() {
        return columnName;
    }

    public String getIdName() {
        return idName;
    }

    public StringBuilder appendToWhere(StringBuilder builder) {
        return builder.append(columnName).append('=').append('\'').append(idName).append('\'');
    }

    @Override
    public String toString() {
        return String.format("SqlIdentifier{idName='%s', columnName='%s'}", idName, columnName);
    }
}
