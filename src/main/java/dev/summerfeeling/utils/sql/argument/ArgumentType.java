package dev.summerfeeling.utils.sql.argument;

public class ArgumentType {

    public static final ArgumentType BIG_INTEGER = new ArgumentType("BIGINT");
    public static final ArgumentType DATETIME = new ArgumentType("DATETIME");
    public static final ArgumentType TIMESTAMP = new ArgumentType("TIMESTAMP");
    public static final ArgumentType BOOLEAN = new ArgumentType("BOOL");
    public static final ArgumentType INTEGER = new ArgumentType("INT");
    public static final ArgumentType BINARY = new ArgumentType("BLOB");
    public static final ArgumentType DOUBLE = new ArgumentType("DOUBLE");
    public static final ArgumentType TEXT = new ArgumentType("TEXT");
    public static final ArgumentType STRING_1000 = new ArgumentType("VARCHAR", 1000);
    public static final ArgumentType STRING_25 = new ArgumentType("VARCHAR", 25);
    public static final ArgumentType STRING_36 = new ArgumentType("VARCHAR", 36);
    public static final ArgumentType STRING_255 = new ArgumentType("VARCHAR", 255);
    public static final ArgumentType STRING_16 = new ArgumentType("VARCHAR", 16);
    public static final ArgumentType STRING_5 = new ArgumentType("VARCHAR", 5);

    private final String value;

    public ArgumentType(String value, int length) {
        this(String.format("%s(%d)", value, length));
    }

    public ArgumentType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
