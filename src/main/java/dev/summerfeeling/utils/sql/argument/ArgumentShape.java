package dev.summerfeeling.utils.sql.argument;

public class ArgumentShape {

    public static final ArgumentShape NOT_NULL = new ArgumentShape("NOT NULL");
    public static final ArgumentShape AUTO_INCREMENT = new ArgumentShape("AUTO_INCREMENT");
    public static final ArgumentShape UNIQUE = new ArgumentShape("UNIQUE");

    public static final ArgumentShape DEFAULT_ZERO = new ArgumentDefaultShape(0);
    public static final ArgumentShape DEFAULT_ONE = new ArgumentDefaultShape(1);
    public static final ArgumentShape DEFAULT_MINUS_ONE = new ArgumentDefaultShape(-1);
    public static final ArgumentShape DEFAULT_NULL = new ArgumentDefaultShape("NULL");

    private final String value;

    public ArgumentShape(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static class ArgumentDefaultShape extends ArgumentShape {

        public ArgumentDefaultShape(int defaultValue) {
            this(Integer.toString(defaultValue));
        }

        public ArgumentDefaultShape(String defaultValue) {
            super(String.format("DEFAULT '%s'", defaultValue));
        }
    }
}
