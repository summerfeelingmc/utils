package dev.summerfeeling.utils.sql.argument;

import com.google.common.base.Joiner;

public class TableArgument {
    private static final Joiner SPACE_JOINER = Joiner.on(' ');

    private final ArgumentType type;
    private final String name;
    private final ArgumentShape[] shapes;

    protected boolean primary = false;

    public TableArgument(String name, ArgumentType types, ArgumentShape... shapes) {
        this.type = types;
        this.name = name;
        this.shapes = shapes;
    }

    public ArgumentType getType() {
        return type;
    }

    public ArgumentShape[] getShapes() {
        return shapes;
    }

    public String getName() {
        return name;
    }

    public TableArgument primary() {
        this.primary = true;
        return this;
    }

    public boolean isPrimary() {
        return primary;
    }

    public String format() {
        return SPACE_JOINER.join(name, type.getValue(), shapes);
    }

    @Override
    public String toString() {
        return format();
    }
}
